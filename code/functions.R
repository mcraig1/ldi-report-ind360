#load capacity definitions & misc data
#cap.definitions = read.csv('./code/cap-def-table.csv')
# cap.ds <- read.delim("C:/reports360/data/cap_data.txt")
# #filter active capacities
# cap.ds = dplyr::filter(cap.ds, cap.ds$cap.status=="Active")
# 
# #user name placeholder
# usr.name = "Leslie Sample"

#convert table comparison values to icons
comp.icons = function (valstring) {
  out = vector()
  for (i in 1:length(valstring)) {
    if (isTRUE(valstring[i])) {
      out[i] = "<img class='comp-icon' src='C:\\reports360\\images\\360-icon.png'/>"
    } else  {
      out[i] = ""
    } 
  }
  return(out)
}

#convert table comparison values to action text
# comp.actions = function (valstring) {
#   out = vector()
#   for (i in 1:length(valstring)) {
#     if (valstring[i] == 0) {
#       out[i] = "grow it"
#     } else if (valstring[i] == 1) {
#       out[i] = "stretch it"
#     } else if (valstring[i] == 2) {
#       out[i] = "own it"
#     }
#   }
#   return(out)
# }

#interpreting your results text transform
interpret.txt = function (r) {
  d = db.capsumm[r,]
  if (isFALSE(d$misaligned)) {
    return ('equivalent to')
  } else if (isTRUE(d$misaligned) & (d$self < d$national)) {
    return ('less than')
  } else {
    return ('greater than')
  }
}

#make "response" plural if needed
response.label = function (n) {
  if(n==0 | n>1) {
    return ("responses")
  } else {
    return ("response")
  }
}

#capacity data table builder
table.builder = function (capcode) {
  
  x = db.capitems[which(db.capitems$capacitycode == db.capsumm$capacitycode[capcode]), 
                  c("supervisor", "peer", "supervisee", "other", "national", "self", "cap_rows", "misaligned")]
  x = cbind(x[,1:6], comp.icons(x[,8]), x[,7])
  
  x[,1:6] = format(x[,1:6], nsmall=2)
 
  
  htmlTable(x,
            header = c("Supervisor", "Peer", "Supervisee", "Other", "Average", "Self", "Alignment", paste("Elements of", db.capsumm$cap[capcode])),
            css.class="ldi-cap-table",
            rnames = FALSE)
}

#function to completely hide bars that have no data 
bar.hide = function (val) {
  if (is.na(val) | val == "NA") {
    return ("display:none;")
  }
}

#function to list-ify the reflection items & add spacing
reflect.builder = function (capcode) {
  x = db.reflect[which(db.reflect$cap_code == capcode),]
  nitems = nrow(db.capitems[which(db.capitems$capacitycode == capcode),])
  
  reflect.list <<- toString(paste0("<tr style='height:", 100/nitems, "%'><td>", x$cap_reflect, "</td></tr>")) |> stringr::str_replace_all("</td></tr>,", "</td></tr>")
  
  
  reflect.padding <<- ifelse(nitems < 10,
                             4.5 - (nitems*.38),
                             .95)
}

# 
# #function to put in correct HTML for the scale backgrounds
# scale.builder = function (scale) {
#   switch(scale,
#          '15' = "<div id='scale-container-5' class='scale-line-container15'><span class='scale-container-number'>5</span><div class='scale-line'><hr class='scale-line-hr'/></div></div><div id='scale-container-4' class='scale-line-container15'><span class='scale-container-number'>4</span><div class='scale-line'><hr class='scale-line-hr'/></div></div><div id='scale-container-3' class='scale-line-container15'><span class='scale-container-number'>3</span><div class='scale-line'><hr class='scale-line-hr'/></div></div><div id='scale-container-2' class='scale-line-container15'><span class='scale-container-number'>2</span><div class='scale-line'><hr class='scale-line-hr'/></div></div><div id='scale-container-1' class='scale-line-container15'><span class='scale-container-number scale-container-number-bttm'>1</span></div>",
#          '03' = "<div id='scale-container-3' class='scale-line-container03'><span class='scale-container-number'>3</span><div class='scale-line'><hr class='scale-line-hr'/></div></div><div id='scale-container-2' class='scale-line-container03'><span class='scale-container-number'>2</span><div class='scale-line'><hr class='scale-line-hr'/></div></div><div id='scale-container-1' class='scale-line-container03'><span class='scale-container-number'>1</span><div class='scale-line'><hr class='scale-line-hr'/></div></div><div id='scale-container-0' class='scale-line-container03'><span class='scale-container-number scale-container-number-bttm'>0</span></div>")
# }



#group header for the "your index" section
# group.switch = function (x) {
#   switch(x,
#          'cap-innol' = 'Innovative Leadership',
#         'cap-sl' = 'Sustainable Leadership',
#          'cap-cl' = 'Civic Leadership',
#         'cap-iperl' = 'Interpersonal Leadership',
#         'cap-incl' = 'Inclusive Leadership',
#         'cap-pl' = 'Personal Leadership'
#          )
# }


#get capacity info
# cap.storage <<- list()
# 
# cap.values = function (capID, id) {
#   cap.storage[[id]] <<- c(name = capID,
#                           grp = cap.ds[cap.ds$cap == capID & cap.ds$cap.group!='', "cap.group"],
#                         def = cap.ds[cap.ds$cap == capID & cap.ds$cap.group!="", 'cap.def'],
#                         reflect = list(paste0("<li>",cap.ds[cap.ds$cap == capID & cap.ds$cap.reflect!="", 'cap.reflect'])),
#                         rows = list(cap.ds[cap.ds$cap == capID & cap.ds$cap.rows!="", 'cap.rows'] |> stringr::str_trim('both')),
#                         )
# }
# 
# 
# #generate dummy data
# dummy.data = function (x) {
#   x = length(x)
#   cbind(format(round(matrix(runif(x*3, 1, 3), nrow=x, ncol=3),2), nsmall=2),
#         sample(0:2,x,replace=TRUE))
# }